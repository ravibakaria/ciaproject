<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/ravi', function () {
//     return view('welcome');
// });
Route::view('/report','reports.show');
Route::get('fees/report', 'StudentController@feesReport');
Route::get('importExport', 'ExcelController@importExport');
Route::get('showdata', 'ExcelController@showdata');
Route::get('downloadExcel/{type}', 'ExcelController@downloadExcel');
Route::post('importExcel', 'ExcelController@importExcel');

Route::post('mail/send', 'MailController@send');

Route::get('/', 'Controller@loadhome');
Route::get('roll/{id}', 'HomeController@roll');
Route::resource('all_rolllist', 'RollController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('courses', 'CourseController');
Route::resource('syllabus', 'SyllabusController');
Route::resource('student', 'StudentController');
Route::get('admission/{id}', 'StudentController@admission');
Route::get('student_detail/{id}', 'StudentController@student_detail');
Route::post('admissionaction/{id}', 'StudentController@admissionaction');
Route::post('Fees/{id}', 'StudentController@fees_history');
Route::post('ajaxRequest', 'StudentController@ajaxRequestPost');
Route::post('ajaxRequestToCourses', 'ToolController@ajaxRequestToCourses');
Route::post('ajaxRequestmail', 'StudentController@ajaxRequestPostMail');
Route::resource('fee', 'FeesController');
Route::resource('aboutus', 'HomepageController');
Route::resource('service', 'ServiceControllerController');
Route::resource('review', 'CustomerReviewController');
Route::get('softdestroy/{id}', 'StudentController@softdestroy');
Route::resource('admin', 'AdminController');
Route::resource('mail', 'MailtemplateController');
Route::get('notif', 'StudentController@notif');
Route::get('changepassword', 'HomeController@changepass');
Route::get('tool', 'ToolController@index');
// Route::get('aboutus', 'Advertise@about');


