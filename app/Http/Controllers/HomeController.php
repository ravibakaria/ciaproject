<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;
use App\Logs;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    public function roll($id){
        $rolls =  User::where('isadmin',$id)->get();
        //dd($rolls);
        // return view('rolls.roll_list',compact('rolls'));
    }
    public function changepass(){
        return view('admin.changepass');
    }
   
}
