<?php
namespace App\Http\Controllers;
 
use App\Http\Controllers\Controller;
use App\Mail\SendMailable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
 
class MailController extends Controller
{
    public function send(Request $request)
    {   
        $request->validate([
            'to'=>'required',
            'subject'=>'required',
            'mailbody'=>'required',
            ]);
        $objDemo = new \stdClass();
        $objDemo->subject = $request->get('subject');
        $objDemo->body = $request->get('mailbody');
       // $objDemo->sender = $request->get('name'); 
        $objDemo->receiver = $request->get('name');
 
        Mail::to($request->get('to'))->send(new SendMailable($objDemo));
       // return 'Email was sent';
       return redirect()->back()->with('success', $request->get('to').' email was sent'); 
    }
} 