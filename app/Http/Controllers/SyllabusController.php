<?php

namespace App\Http\Controllers;

use App\Syllabus;
use App\Course;
use \DB;
use App\Logs;
use Auth;
use Illuminate\Http\Request;

class SyllabusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
         $syllabus = DB::select("SELECT syllabi.*,courses.name FROM syllabi LEFT JOIN courses ON courses.id = syllabi.course_id");
        
         return view('Syllabus.Syllabus_list', compact('syllabus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $courses = Course::all();
       
        return view('Syllabus.Syllabus_add', compact('courses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
        'syllabus'=>'required',
        'course'=>'required'
        ]);
        $syllabus = new Syllabus([
        'course_id'=>  $request->get('course'),
        'syllabus'=> $request->get('syllabus')
        ]);
       
        $syllabus->save();
        $uid = Auth::user()->id;
        $log = new Logs([
            'user_id'=>  $uid,
            'msg'=>'New Syllabus Added',
            'created_at'=> date('Y-m-d H:i:s')
        ]);
       $log->save();
        return redirect('/syllabus')->with('success', 'Syllabus has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Syllabus  $syllabus
     * @return \Illuminate\Http\Response
     */
    public function show(Syllabus $syllabus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Syllabus  $syllabus
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $courses = Course::all();
        $syllabus = Syllabus::find($id);
        $data = ['courses'=>$courses,'syllabus'=>$syllabus];

        return view('Syllabus.Syllabus_edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Syllabus  $syllabus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'syllabus'=>'required',
            'course'=>'required'
          ]);
    
          $syllabus = Syllabus::find($id);
          $syllabus->course_id = $request->get('course');
          $syllabus->syllabus = $request->get('syllabus');
         
          $syllabus->save();
          $uid = Auth::user()->id;
          $log = new Logs([
              'user_id'=>  $uid,
              'msg'=>'Syllabus Update',
              'created_at'=> date('Y-m-d H:i:s')
          ]);
         $log->save();
          return redirect('/syllabus')->with('success', 'Syllabus has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Syllabus  $syllabus
     * @return \Illuminate\Http\Response
     */
    public function destroy(Syllabus $syllabus)
    {
        //
        $uid = Auth::user()->id;
        $log = new Logs([
            'user_id'=>  $uid,
            'msg'=>'Syllabus Delete',
            'created_at'=> date('Y-m-d H:i:s')
        ]);
       $log->save();
    }
}
