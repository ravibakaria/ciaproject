<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Fees;
use App\Logs;
use Auth;
use \DB;
class FeesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        echo "fees index method";
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'sid'=>'required',
            'cid'=>'required',
            'pdate'=>'required',
            'fees'=>'required'
            
            ]);
            $fees = new Fees([
                'student_id'=> $request->get('sid'),
                'course_id'=>  $request->get('cid'),
                'tdate'=>  $request->get('pdate'),
                'fees'=> $request->get('fees')
            ]);
            $fees->save();
            $uid = Auth::user()->id;
            $log = new Logs([
                'user_id'=>  $uid,
                'msg'=>'New Fees Added',
                'created_at'=> date('Y-m-d H:i:s')
            ]);
           $log->save();


           
            return redirect()->back()->with('success', 'fees paid successfully');   
       // print_r($_POST); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
