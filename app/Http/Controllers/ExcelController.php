<?php
 
namespace App\Http\Controllers;
 
use App\Item;
use App\Student;
use App\Course;
use App\Admission;
use App\Fees;
use DB;
use Excel;
use Illuminate\Http\Request;
 
class ExcelController extends Controller
{
 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function importExport()
    {
        return view('inc.demo');
    }
 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function downloadExcel($type)
    {
        $data = Item::get()->toArray();
            
        return Excel::create('itsolutionstuff_example', function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download($type);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function importExcel(Request $request)
    {   
        header( 'Content-Type: text/html; charset=utf-8' ); 
        $request->validate([
            'import_file' => 'required'
        ]);
 
        $path = $request->file('import_file')->getRealPath();
        $data = Excel::load($path)->get();
        if($data->count()){
            foreach ($data as $key => $value) {
               
                    if($value->gender=='male'){
                        $gender='1';
                    }
                    if($value->gender=='female'){
                        $gender = '0';
                    }
                    Student::create([
                        'name'=>   $value->name,
                        'email'=>  $value->email,
                        'phone'=>  $value->phone,
                        'status'=> 1,
                        'course'=>  $value->course,
                        'reference'=>  NULL,
                        'address'=>  $value->address,
                        'pin'=>  $value->pin,
                        'city'=>  $value->city,
                        'gender'=>  $gender
                    ]);
                    $studen_id = DB::getPdo()->lastInsertId();
                    $c_id = $value->course;
                    $feess = Course::find($c_id);
                    $fees1 =  $feess->fees;
                    //$admission = new Admission([
                        Admission::create([
                        'student_id'=>  $studen_id,
                        'course_id'=>  $value->course,
                        'date'=> $value->jdate,
                        'fees'=>$fees1
                        
                        ]);
                    $admission_id = DB::getPdo()->lastInsertId();
                    //$fees = new Fees([
                        Fees::create([
                            'student_id'=> $studen_id, 
                            'admission_id'=>  $admission_id,
                            'course_id'=>  $value->course,
                            'tdate'=> $value->jdate,
                            'fees'=> $value->fees
                        ]);
        }}
 
        return back()->with('success', 'Insert Record successfully.');
    }
    public function showdata(){
       // mysql_set_charset('utf8');
        $data = Item::all();
       // dd($data);
        return view('inc.show', compact('data'));
    }
}