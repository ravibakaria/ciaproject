<?php

namespace App\Http\Controllers;
use App\Mail\SendMailable;
use Illuminate\Support\Facades\Mail;
use App\Student;
use App\Course;
use App\Admission;
use App\Fees;
use App\Logs;
use \DB;
use Auth;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {   
        
         $students = DB::table('students')
         ->select('students.name','students.status','students.email','students.reference','students.phone','students.id', DB::raw('COUNT(courses.name) as c_count'),DB::raw('sum(admissions.fees) as fees1'))
         ->join('courses','students.course','=','courses.id')
         ->join('admissions','students.id','=','admissions.student_id')
         ->groupBy('students.id', 'students.name','students.email','students.status','students.reference','students.phone')
         ->where('students.status','1')
         ->orderby('students.id','dese')
         ->get();
        // ->paginate(4);
         //$query=$students->toSql();
       // print_r($query);exit;
        return view('student.list_student', compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $courses = Course::all();
        $students = Student::all();
        // $students = Student::where('status', '=', 'NULL')->get();
        return view('student.add_student', compact('courses','students'));
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $uid = Auth::user()->id;
        //dd($uid);
        $c_id = $request->get('course');
        $feess = Course::find($c_id);
        $fees1 =  $feess->fees;
        $c_name = $feess->name;
      // print_r($_POST);exit();
       $request->validate([
        'name'=>'required',
        'email'=>'required',
        'phone'=>'required',
        'course'=>'required',
        'jdate'=>'required',
        'fees'=>'required',
        'pin'=>'required',
        'city'=>'required',
        'address'=>'required',
        'gender'=>'required',
        ]);
        

        $student = new Student([
        'name'=>  $request->get('name'),
        'email'=>  $request->get('email'),
        'phone'=>  $request->get('phone'),
        'status'=> 1,
        'course'=>  $request->get('course'),
        'reference'=>  $request->get('refe'),
        'address'=>  $request->get('address'),
        'pin'=>  $request->get('pin'),
        'city'=>  $request->get('city'),
        'gender'=>  $request->get('gender')
       
        ]);
        //print_r($student);dd($student);
        $student->save();
       

        $studen_id = DB::getPdo()->lastInsertId();
        $admission = new Admission([
            'student_id'=>  $studen_id,
            'course_id'=>  $request->get('course'),
            'date'=>  $request->get('jdate'),
            'fees'=>$fees1
            
            ]);
        $admission->save();
        $admission_id = DB::getPdo()->lastInsertId();
        $fees = new Fees([
                'student_id'=> $studen_id, 
                'admission_id'=>  $admission_id,
                'course_id'=>  $request->get('course'),
                'tdate'=>  $request->get('jdate'),
                'fees'=> $request->get('fees')
            ]);
            $fees->save();
        $uid = Auth::user()->id;
        $log = new Logs([
            'user_id'=>  $uid,
            'msg'=>'New student add',
            'created_at'=> date('Y-m-d H:i:s')
        ]);
       $log->save();
    //    $pending = $fees1 - $request->get('fees');
    //    $objDemo = new \stdClass();
    //    $objDemo->fees = $request->get('fees');
    //    $objDemo->course = $c_name;
    //    $objDemo->pending = $pending;
    //    $objDemo->totalfees = $fees1;
    //    $objDemo->sender = 'Priyanka Bagde';
    //    $objDemo->receiver = $request->get('name');
    //    Mail::to($request->get('email'))->send(new SendMailable($objDemo));
       return redirect('/student')->with('success', 'Student has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $courses = Course::all();
        $student = Student::where('status','1')->where('id',$id)->firstOrFail();
        $data = ['courses'=>$courses,'student'=>$student];
        return view('student.edit_student', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
       $request->validate([
        'name'=>'required',
        'email'=>'required',
        'phone'=>'required'
        ]);
        

        $student = Student::find($id);
          $student->name = $request->get('name');
          $student->email = $request->get('email');
          $student->phone = $request->get('phone');
          $student->save();
          $uid = Auth::user()->id;
          $log = new Logs([
              'user_id'=>  $uid,
              'msg'=>'Student Update',
              'created_at'=> date('Y-m-d H:i:s')
          ]);
         $log->save();
          return redirect('/student')->with('success', 'Student has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $id)
    {
        $student = Student::find($id);
        $student->status = '0';
        $student->save();
        return redirect('/student')->with('success', 'Student has been Deleted');
    }
    public function softdestroy($id)
    {
        $student = Student::find($id);
        $student->status = '0';
        $student->save();
        $uid = Auth::user()->id;
        $log = new Logs([
            'user_id'=>  $uid,
            'msg'=>'Student Delete',
            'created_at'=> date('Y-m-d H:i:s')
        ]);
       $log->save();
        return redirect('/student')->with('success', 'Student has been Deleted');
    }
    public function admission($id)
    {
        $courses = Course::all();
        $student = Student::find($id);
        $data = ['courses'=>$courses,'student'=>$student];

        return view('student.newadmission', compact('data'));
    }
    public function admissionaction(Request $request,$id){
            // print_r($_POST);exit();
            
       $request->validate([
        'name'=>'required',
        'email'=>'required',
        'phone'=>'required',
        'course'=>'required',
        'jdate'=>'required',
        'fees'=>'required',
        'course'=>'required'
        ]);
        $c_id = $request->get('course');
            $student = Course::find($c_id);
            $fees =  $student->fees;

        $admission = new Admission([
            'student_id'=>  $id,
            'course_id'=>  $request->get('course'),
            'date'=>  $request->get('jdate'),
            'fees'=> $fees
            ]);
        $admission->save();
        $admission_id = DB::getPdo()->lastInsertId();
        $fees = new Fees([
                'student_id'=> $id, 
                'admission_id'=>  $admission_id,
                'course_id'=>  $request->get('course'),
                'tdate'=>  $request->get('jdate'),
                'fees'=> $request->get('fees')
            ]);
            $fees->save();
            $uid = Auth::user()->id;
            $log = new Logs([
                'user_id'=>  $uid,
                'msg'=>'New Admission Added',
                'created_at'=> date('Y-m-d H:i:s')
            ]);
           $log->save();
        return redirect('/student')->with('success', 'New Course added');
    }

    public function student_detail($id){
        $sname = student::find($id);
        //print_r($sname);exit();
        // $balance = DB::table('fees')
        //              ->select(DB::raw('sum(fees) as fees'))
        //              ->where('course_id', '=', '1')
        //              ->where('student_id', '=', '1')
        //              ->first();
        //              echo $balance->fees;
        //              print_r($balance);
        
        // exit;
//$students = ['Course'=>$Course,'Fees'=>$Fees,'Admissions'=>$Admissions];
// echo "<pre>"; 
// print_r($result);exit; 
         $student = DB::select("SELECT courses.name as name,courses.id as c_id,admissions.fees as fees,admissions.date as date FROM courses LEFT JOIN admissions on courses.id = admissions.course_id WHERE admissions.student_id = '$id'");
         $students = ['sname'=>$sname,'student'=>$student];
        return view('student.detail_student', compact('students'));
    }
    public function fees_history(Request $request,$id){
        $s_id = $request->get('s_id');
        // $students = DB::select("");
        $fees = DB::table('fees')->where('student_id', '=', $s_id)
                    ->where('course_id', '=', $id)
                    ->get(); 

            // $fees = DB::table('fees')
            // ->where([
            //         ['student_id', '=', $s_id],
            //         ['course_id', '=', $id]
            //     ])->get();
                echo "<pre>";
        print_r($fees);
    }

    public function ajaxRequestPost(){
        $s_id = $_POST['s_id'];
        $id = $_POST['c_id'];
        $students = DB::select("SELECT * FROM `fees` WHERE student_id ='$s_id' AND course_id = '$id'");
       return view('student.fees_history', compact('students'));
    }

    public function editadmission($id)
    {
        $courses = Course::all();
        
        $students = DB::select("SELECT * FROM `admissions` WHERE student_id ='$id' AND course_id = '$id'");
        $data = ['courses'=>$courses,'student'=>$student];
        
        return view('student.edit_student', compact('data'));
    }
    public function notif()
    {   
        $logs = DB::select("SELECT logs.*,users.name from logs LEFT JOIN users ON users.id = logs.user_id ORDER BY logs.id DESC LIMIT 10");        //dd($logs);
        return view('notif.notif',compact('logs'));
    }
    public function ajaxRequestPostMail(){
        $mail_id = $_POST['id'];
        $mails = \App\Mailtemplate::where('id',$mail_id)->firstOrFail();
        echo json_encode($mails);
    }
    public function feesReport(){
        
    }
}
 