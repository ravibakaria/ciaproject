<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Roll;
use App\User;
use \DB;
use App\Logs;
class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        //$users = User::where('isadmin','!=','1')->firstOrFail();
        $users = DB::select("SELECT users.*,rolls.roll_name from users LEFT JOIN rolls on rolls.id = users.isadmin where users.isadmin != '1'");
       // dd($users);
        return view('admin.list_admin',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rolls = Roll::all();
        return view('admin.add_admin', compact('rolls'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'isadmin' => ['required'],
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required | string |',
            'email'=>'required |unique:users',
            'isadmin'=>'required',
            'password'=>'required | confirmed | min:6',
            ]);
        $user = new User([
            'name'=>  $request->get('name'),
            'email'=>  $request->get('email'),
            'isadmin'=>  $request->get('isadmin'),
            'password'=> Hash::make($request->get('password'))
            ]);
           
          // dd($user);
            $user->save();
            return redirect('/admin')->with('success', 'New User has been added');
           
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {  
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        //$rolls = Roll::all();
        $rolls = DB::select("SELECT * from rolls where id != 1");
        //dd($rolls->id);
        $user = User::find($id);
      return view('admin.edit_admin',compact('user','rolls'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //echo $id;exit;
        
            $user = User::find($id);
            $user->name = $request->get('name');
            $user->email = $request->get('email');
            $user->isadmin = $request->get('isadmin');
           // dd($user);
            $user->save();
            return redirect('/admin')->with('success', 'admin has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
