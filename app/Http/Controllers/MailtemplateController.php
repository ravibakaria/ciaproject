<?php

namespace App\Http\Controllers;

use App\Mailtemplate;
use Illuminate\Http\Request;

class MailtemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mails = Mailtemplate::all();
        return view('mails.list_mail',compact('mails'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('mails.create_mail');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $request->validate([
            'title'=>'required',
            'subject'=>'required',
            'body'=>'required',
            ]);

            $mail = new Mailtemplate([
                'title'=>  $request->get('title'),
                'subject'=>  $request->get('subject'),
                'body'=>  $request->get('body')
               
                ]);
                //print_r($student);dd($student);
                $mail->save();
                return redirect('/mail')->with('success', 'Mail Tamplate has been added');
            
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Mailtemplate  $mailtemplate
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $mails = Mailtemplate::find($id);
        //dd($mails);
        return view('mails.show_mail',compact('mails'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Mailtemplate  $mailtemplate
     * @return \Illuminate\Http\Response
     */
    public function edit(Mailtemplate $mailtemplate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mailtemplate  $mailtemplate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mailtemplate $mailtemplate)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Mailtemplate  $mailtemplate
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mailtemplate $mailtemplate)
    {
        //
    }
}
