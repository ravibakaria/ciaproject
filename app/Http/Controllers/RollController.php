<?php

namespace App\Http\Controllers;

use App\Roll;
use App\Logs;
use Auth;
use \DB;
use Illuminate\Http\Request;

class RollController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $rolls = Roll::all();
        return view('rolls.all_rolllist',compact('rolls'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
       
        return view('rolls.create_roll');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $request->validate([
            'name'=>'required'
            ]);
            
    
            $roll = new Roll([
            'roll_name'=>  $request->get('name')
            ]);
            //print_r($student);dd($student);
            $roll->save();
        
        $uid = Auth::user()->id;
        $log = new Logs([
            'user_id'=>  $uid,
            'msg'=>'New Role Added',
            'created_at'=> date('Y-m-d H:i:s')
        ]);
       $log->save();
       return redirect('/all_rolllist')->with('success', 'Role has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Roll  $roll
     * @return \Illuminate\Http\Response
     */
    public function show(Roll $roll)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Roll  $roll
     * @return \Illuminate\Http\Response
     */
    public function edit($id)    
    {  
       $roll = Roll::where('id',$id)->firstOrFail();
      
        return view('rolls.edit_roll',compact('roll'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Roll  $roll
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {   
        //print_r($id);exit;
        $request->validate([
            'name'=>'required'
            ]);
            
            $roll = Roll::find($id);
            $roll->roll_name = $request->get('name');
        
            $roll->save();
        $uid = Auth::user()->id;
        $log = new Logs([
            'user_id'=>  $uid,
            'msg'=>'Role Edit',
            'created_at'=> date('Y-m-d H:i:s')
        ]);
       $log->save();
       return redirect('/all_rolllist')->with('success', 'Role has been Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Roll  $roll
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $roll = Roll::find($id);
        $roll->delete();
        $uid = Auth::user()->id;
        $log = new Logs([
            'user_id'=>  $uid,
            'msg'=>'Role Delete',
            'created_at'=> date('Y-m-d H:i:s')
        ]);
       $log->save();
       return redirect('/all_rolllist')->with('success', 'Role has been Deleted');
    }
}
