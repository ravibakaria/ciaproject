<?php

namespace App\Http\Controllers;

use App\Course;
use Illuminate\Http\Request;
use \DB;
use Auth;
use App\Logs;
class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $courses = Course::all();

        return view('courses.list_courses', compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function create()
    {
        return view('courses.add_courses');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   //print_r($_POST);exit;
        $request->validate([
            'name'=>'required',
            'duration'=> 'required|integer',
            'fees' => 'required|integer',
            'level' => 'required'
          ]);
          $course = new Course([
            'name' => $request->get('name'),
            'duration'=> $request->get('duration'),
            'fees'=> $request->get('fees'),
            'level'=> $request->get('level')
          ]);
          $course->save();
          $uid = Auth::user()->id;
          $log = new Logs([
              'user_id'=>  $uid,
              'msg'=>'course added',
              'created_at'=> date('Y-m-d H:i:s')
          ]);
          $log->save();
          return redirect('/courses')->with('success', 'Course has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $course = DB::select("SELECT syllabi.*,courses.name FROM syllabi LEFT JOIN courses ON courses.id = syllabi.course_id WHERE courses.id = '$id'");
        if(count($course)>0){
            return view('courses.singel_courses', compact('course'));
        }else{
            abort(404);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $course = Course::findorFail($id);

        return view('courses.edit_courses', compact('course'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
            'duration'=> 'required',
            'fees' => 'required|integer'
          ]);
    
          $course = Course::find($id);
          $course->name = $request->get('name');
          $course->duration = $request->get('duration');
          $course->fees = $request->get('fees');
          $course->save();
          $uid = Auth::user()->id;
          $log = new Logs([
              'user_id'=>  $uid,
              'msg'=>'course updated',
              'created_at'=> date('Y-m-d H:i:s')
          ]);
          $log->save();
          return redirect('/courses')->with('success', 'Course has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        //echo $id; exit;
        $course = Course::find($id);
        $course->delete();
        $uid = Auth::user()->id;
        $log = new Logs([
            'user_id'=>  $uid,
            'msg'=>'course Delete',
            'created_at'=> date('Y-m-d H:i:s')
        ]);
       $log->save();
        return redirect('/courses')->with('success', 'Course has been deleted Successfully');
    }
 
}
