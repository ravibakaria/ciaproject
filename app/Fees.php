<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fees extends Model
{
    protected $fillable = [
        'student_id',
        'course_id',
        'tdate',
        'fees'
        
      ];
}
