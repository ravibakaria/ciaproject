<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [
        'name',
        'email',
        'phone',
        'course',
        'reference',
        'status',
        'address',
        'city',
        'pin',
        'gender'
        
      ];

      public function fees()
      {
          return $this->hasMany('App\Fees', 'stusent_id', 'stusent_id');
      }

      public function admission()
      {
          return $this->hasMany('App\Admission', 'stusent_id', 'stusent_id');
      }
}
