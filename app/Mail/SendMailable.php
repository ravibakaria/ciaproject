<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailable extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $demo;
    public function __construct($demo)
    {
        $this->demo = $demo;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
   
    public function build()
    {   
       // echo $this->demo->body;exit;
        return $this->from('codeinsightacademy@gmail.com', 'Code insightac ademy')
                    ->subject($this->demo->subject)
                    ->view('mails.demo');
                   
    }
}
