<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admission extends Model
{
    protected $fillable = [
        'student_id',
        'course_id',
        'date',
        'fees'
        
      ];

      public function student()
      {
          return $this->belongsTo('App\Student', 'id', 'id');
      }
}
