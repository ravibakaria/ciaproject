@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
            
                <div class="card-header">
                    <h2>{{$mails['title']}}</h2>
                </div> 
                <div class = "card-body">
               
                <div class="well">{{$mails->subject}}
                
                <div>{{$mails->body}}</div>

                </div>
                
                </div>  
            </div>
        </div>
    <div>
</div>
@endsection