@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            <div class="card-header">New Mail Tamplate
                <a class="nav-link" href="{{ URL::previous() }}">{{ __('Back') }}</a>
            </div>
            <div class="card-body">
            <form name="myForm" id="myForm" action="{{URL ('/mail') }}" method="POST">
            @csrf
                <div class="form-group"> 
                    <label for="title">Mail Title</label>
                    <input tept = "text"  class="form-control" name ="title">
                </div>
                <div class="form-group">
                    <label for="subject">Subject</label>
                    <input tept = "text"  class="form-control" name ="subject">
                </div>
                <div class="form-group">
                    <label for="body">Mail Body</label>
                    <textarea  class="form-control" name ="body" rows = "12"></textarea>
                </div>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    <div>
</div>
<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('vendor/unisharp/laravel-ckeditor/adapters/jquery.js') }}"></script>
<script>
     $('textarea').ckeditor();;
</script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
   <script>
    $(document).ready(function () {
    $('#myForm').validate({ // initialize the plugin
        rules: {
            title: {
                required: true
            },
            subject: {
                required: true
            },
            body: {
                required: true
            }
        },
        messages:{
            title: {
                required: "Title is required"
            },
            subject: {
                required: "Subject is required"
                
            },
            body: {
                required: "Mail Body  is required"
            }
        }
    });
   
});
</script>
@endsection