@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
            @if(session()->get('success'))
                        <div class="alert alert-success">
                        {{ session()->get('success') }}  
                        </div><br />
            @endif
                <div class="card-header">
                    <h2>List Of Mail Tamplates</h2>
                    <a class="nav-link" href="{{ URL ('mail/create') }}">{{ __('New Mail Tamplate') }}</a>
                </div> 
                <div class = "card-body">
                @foreach($mails as $mail)
                <div class="well"><h2>{{$mail->title}}</h2>
                <div><a href="{{URL ('mail/'.$mail->id)}}">Show</a></div>

                </div>
                @endforeach
                </div>  
            </div>
        </div>
    <div>
</div>
@endsection