@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            <div class="card-header">New Mail Tamplate
                <a class="nav-link" href="{{ URL::previous() }}">{{ __('Back') }}</a>
            </div>
            <div class="card-body">
            <form>
                <div class="form-group">
                    <label for="title">Mail Title</label>
                    <input tept = "text"  class="form-control" name ="title">
                </div>
                <div class="form-group">
                    <label for="subject">Subject</label>
                    <input tept = "text"  class="form-control" name ="subject">
                </div>
                <div class="form-group">
                    <label for="body">Mail Body</label>
                    <textarea  class="form-control" name ="body" rows = "12"></textarea>
                </div>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    <div>
</div>
<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('vendor/unisharp/laravel-ckeditor/adapters/jquery.js') }}"></script>
<script>
     $('textarea').ckeditor();;
</script>
@endsection