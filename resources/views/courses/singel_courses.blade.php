@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h4>{{ $course[0]->name }}</h4>
                
                <a class="nav-link" href="{{ URL::previous() }}">{{ __('Back') }}</a>
                </div>

                <div class="card-body">
                    <div class="panel-body">
                        {!! $course[0]->syllabus!!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection