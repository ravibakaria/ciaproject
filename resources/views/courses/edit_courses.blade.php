@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Courses
                <a class="nav-link" href="{{ URL::previous() }}">{{ __('Back') }}</a>
                </div>

                <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
                @endif
                <form name="myForm" id="myForm" method="post" action="{{ route('courses.update', $course->id) }}">
                    @method('PATCH')
                    @csrf
                    <div class="form-group">
                        <label for="name">Courses Name</label>
                        <input type="text" name = "name" class="form-control" id="name" aria-describedby="emailHelp" placeholder="Enter Courses Name"  value={{ $course->name }}>
                    </div>
                    <div class="form-group">
                        <label for="duration">Duration</label>
                        <input type="text"  name = "duration" class="form-control" id="duration" placeholder="Enter Duration"  value={{ $course->duration }}>
                    </div>
                    <div class="form-group">
                    <label for="free">Fees</label>
                        <input type="text"  name = "fees" class="form-control" id="free" placeholder="Enter Free"  value={{ $course->fees }}>
                    </div>
                    <button type="submit" class="btn btn-primary">Update</button>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>
$(document).ready(function () {
$('#myForm').validate({ // initialize the plugin
 rules: {
     name: {
         required: true
        
     },
     duration: {
         required: true
     },
     fees: {
         required: true
     },
     level: {
         required: true
     }
 },
 messages: {
    name: {
        required: "Course Name is required"
        
    },
    duration: {
        required: "Duration is required"
    },
    fees: {
        required: "Fees field never blank"
    },
    level: {
        required: "Level is required"
    }
}
});

});
</script>

@endsection