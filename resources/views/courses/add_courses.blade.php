@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Add New Courses
                <a class="nav-link" href="{{ URL::previous() }}">{{ __('Back') }}</a>
               
                </div>

                <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
                @endif
                <form name="myForm" id="myForm"  action="{{URL ('courses') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="name">Courses Name</label>
                        <input type="text" name = "name" class="form-control" id="name" aria-describedby="emailHelp" placeholder="Enter Courses Name">
                    </div>
                    <div class="form-group">
                        <label for="duration">Duration</label>
                        <input type="text" name = "duration" class="form-control" id="duration" placeholder="Enter Duration">
                    </div>
                    <div class="form-group">
                    <label for="fees">Fees</label>
                        <input type="text" name = "fees" class="form-control" id="fees" placeholder="Enter Fees">
                    </div>
                    <div class="form-group" >
                            <label for="fees">Level</label>
                            <select class="form-control" name='level' id='level'>
                                <option value=''>Select</option>
                                <option value='basic'>Basic</option>
                                <option value='pro'>Pro</option>
                                <option value='premium'>Premium</option>
                            </select>
                       </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
    $(".ref").click(function(){
        
        $("#refe").css("display","block");
        $(".ref").css("display","none");
    });
    
});
</script>
       <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
       <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
   <script>
    $(document).ready(function () {
    $('#myForm').validate({ // initialize the plugin
        rules: {
            name: {
                required: true
               
            },
            duration: {
                required: true
            },
            fees: {
                required: true
            },
            level: {
                required: true
            }
        },
        messages: {
            name: {
                required: "Course Name is required"
               
            },
            duration: {
                required: "Duration is required"
            },
            fees: {
                required: "Fees field never blank"
            },
            level: {
                required: "Level is required"
            }
        }
    });
   
});
</script>

@endsection