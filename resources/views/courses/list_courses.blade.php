@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                <h2>List Of Courses</h2>
                <a class="nav-link" href="{{ URL ('courses/create') }}">{{ __('Add Courses') }}</a>
                </div>

                <div class="card-body">
                    <style>
                    .uper {
                        margin-top: 0px;
                    }
                    </style>
                    <div class="uper">
                    @if(session()->get('success'))
                        <div class="alert alert-success">
                        {{ session()->get('success') }}  
                        </div>
                    @endif
                    <table class="table-responsive">
                        
                    <thead>
                        <tr>
                            <th>SR</th>
                            <th>Courses Name</th>
                            <th>Courses Level</th>
                            <th>Duration</th>
                            <th>Fees</th>
                           
                            <th colspan="2">Action</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i = 0;?>
                        @foreach($courses as $course)
                        <?php $i++;?>
                        <tr>
                            <td>{{$i}}</td>
                            <td><a href="{{ route('courses.show',$course->id)}}" >{{$course->name}}</a> </td>
                            <td>{{$course->level}}</td>
                            <td>{{$course->duration}} hours</td>
                            <td>Rs. {{$course->fees}}/-</td>
                            
                            <td><a href="{{ route('courses.edit',$course->id)}}" class="btn btn-primary">Edit</a></td>
                            <td >
                                <form action="{{ route('courses.destroy', $course->id)}}" method="post" id ="myForm">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" type="submit" id="submit">Delete</button>
                                </form>
                            </td>
                            
                        </tr>
                        @endforeach
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function() {
  
    $('#myForm').submit(function() {
     
    var c = confirm("Click OK to continue?");
    return c; //you can just return c because it will be true or false
    });
});
</script>

@endsection