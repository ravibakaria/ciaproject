@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">New Admission
                <a class="nav-link" href="{{ URL::previous() }}">{{ __('Back') }}</a>
                </div>

                <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
                @endif
                <form id="logout-form" action="{{URL ('student') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="name">Student Name</label>
                        <input type="text" name = "name" class="form-control" id="name" aria-describedby="emailHelp" placeholder="Enter Student Name">
                    </div>
                    <div class="form-group">
                        <label for="name">Student Email</label>
                        <input type="email" name = "email" class="form-control" id="name" aria-describedby="emailHelp" placeholder="Enter Student Name">
                    </div>
                    <div class="form-group">
                        <label for="name">Student Phone No.</label>
                        <input type="text" name = "phone" class="form-control" id="name" aria-describedby="emailHelp" placeholder="Enter Student Phone No.">
                    </div>
                    <div class="form-group">
                        <label for="name">Courses Name</label>
                        <select name="course" class="form-control">
                        <option value="">--Select--</option>
                        @foreach ($courses as $course)
                        <option value="{{$course->id}}">{{ $course->name }}</option>
                        @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                    <label for="fees">Join Date</label>
                        <input type="date" name = "jdate" class="form-control" id="fees" placeholder="Enter Join Date">
                    </div>
                    <div class="form-group">
                    <label for="fees">Fees</label>
                        <input type="text" name = "fees" class="form-control" id="fees" placeholder="Enter Fees">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection