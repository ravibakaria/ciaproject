@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h1>What customer says ?</h1>
                    <a class="nav-link" href="{{ URL::previous() }}">{{ __('Back') }}</a>
                </div>

                <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
                @endif
                <form id="logout-form" action="{{URL ('about_us') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        {{-- <label for="name">Courses Name</label> --}}
                        {{-- <select name="course" class="form-control">
                        <option value="">--Select--</option>
                        @foreach ($courses as $course)
                        <option value="{{$course->id}}">{{ $course->name }}</option>
                        @endforeach
                        </select> --}}  
                    </div>
                    <div class="form-group">
                        <label for="syllabus"><h2>What customer says?</h2></label>
                        <textarea name="aboutus" class="form-control" id="summary-ckeditor" cols="30" rows="10"></textarea>
                    </div>
                    
                   
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('vendor/unisharp/laravel-ckeditor/adapters/jquery.js') }}"></script>
<script>
     $('textarea').ckeditor();;
</script>

@endsection