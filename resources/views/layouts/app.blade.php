<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <!-- <script src="{{ asset('public/js/app.js') }}" ></script> -->
    
    

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet"> --}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    {{-- <link href="{{ asset('css/style.css') }}" rel="stylesheet"> --}}
        {{-- <script src="{{ asset('public/js/app.js') }}" ></script> --}}
   
	<style>
	/* .navbar {
    margin-bottom: 0;
    background-color: #4878c4;
    z-index: 9999;
    border: 0;
    font-size: 12px !important;
    line-height: 1.42857143 !important;
    letter-spacing: 4px;
    border-radius: 0;
    font-family: Montserrat, sans-serif;
  } */
  footer,.card{
      top:100px;
     
  }
  .error{
      color: red;
  }
  /* .navbar-nav li a:hover, .navbar-nav li.active a {
    color: #f4511e !important;
    background-color: #fff !important;
  } .navbar-nav li a {   color: #fff !important;}
  .navbar-brand {   color: #fff !important;} */
  #loader{
            transition:all .3s ease-in-out;
            opacity:1;
            visibility:visible;
            position:fixed;
            height:100vh;width:100%;
            background:#fff;z-index:90000
        }
        #loader.fadeOut{
            opacity:0;visibility:hidden
            }
            .spinner{
                width:40px;
                height:40px;
                position:absolute;
                top:calc(50% - 20px);
                left:calc(50% - 20px);
                background-color:#333;
                border-radius:100%;
                -webkit-animation:sk-scaleout 1s infinite ease-in-out;
                animation:sk-scaleout 1s infinite ease-in-out}
                @-webkit-keyframes sk-scaleout{
                    0%{
                        -webkit-transform:scale(0)
                        }
                        100%{
                            -webkit-transform:scale(1);opacity:0
                            }
                        }
                        @keyframes sk-scaleout{
                            0%{
                                -webkit-transform:scale(0);
                                transform:scale(0)
                                }
                                100%{
                                    -webkit-transform:scale(1);
                                    transform:scale(1);
                                    opacity:0
                                    }
                                }

    
	.modal.right .modal-dialog {
		position: fixed;
		margin: auto;
		width: 320px;
		height: 100%;
		-webkit-transform: translate3d(0%, 0, 0);
		    -ms-transform: translate3d(0%, 0, 0);
		     -o-transform: translate3d(0%, 0, 0);
		        transform: translate3d(0%, 0, 0);
	}

	
	.modal.right .modal-content {
		height: 100%;
		overflow-y: auto;
	}
	
	
	.modal.right .modal-body {
		padding: 15px 15px 80px;
	}


	
	
        
   
/*Right*/
.modal.right.fade .modal-dialog {
		right:0px;
		-webkit-transition: opacity 0.3s linear, right 1.3s ease-out;
		   -moz-transition: opacity 0.3s linear, right 1.3s ease-out;
		     -o-transition: opacity 0.3s linear, right 1.3s ease-out;
		        transition: opacity 0.3s linear, right 1.3s ease-out;
	}
	
	.modal.right.fade.in .modal-dialog {
		right: 0;
	}

/* ----- MODAL STYLE ----- */
	.modal-content {
		border-radius: 0;
		border: none;
	}

	.modal-header {
		border-bottom-color: #EEEEEE;
		background-color: #FAFAFA;
	}

/* ----- v CAN BE DELETED v ----- */




.btn-demo {
	margin: 15px;
	padding: 10px 15px;
	border-radius: 0;
	font-size: 16px;
	background-color: #FFFFFF;
}



	</style>
</head>
<body>
    <div id="app">
        {{-- <div id="loader">
                <div class="spinner">
                </div>
        </div> --}}
        <script>
            // $(document).ready(function(){
            //     setTimeout(() => {
            //         loader.classList.add('fadeOut');
            //         }, 300);
            //     });
        // window.addEventListener('load', () => {
        // const loader = document.getElementById('loader');
        //             setTimeout(() => {
        //             loader.classList.add('fadeOut');
        //             }, 300);
        //         });
        </script>
        <div class="sidebar">
                <div class="sidebar-inner">
                    <div class="sidebar-logo">
                        <div class="peers ai-c fxw-nw">
                            <div class="peer peer-greed">
                                <a class="sidebar-link td-n" href="https://colorlib.com/polygon/adminator/index.html" class="td-n">
                                    <div class="peers ai-c fxw-nw">
                                        <div class="peer">
                                            <div class="logo">
                                                <img src="assets/static/images/logo.png" alt="">
                                            </div>
                                        </div>
                                        <div class="peer peer-greed">
                                            <h5 class="lh-1 mB-0 logo-text">
                                                Adminator
                                            </h5>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="peer">
                                <div class="mobile-toggle sidebar-toggle">
                                    <a href="" class="td-n"><i class="ti-arrow-circle-left"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ul class="sidebar-menu scrollable pos-r">
                       
                        <li class="nav-item mT-30 active"><a class="sidebar-link"  href="{{ URL ('home') }}" ><span class="icon-holder"><i class="c-blue-500 ti-home"></i> </span><span class="title">Dashboard</span></a>
                        </li>
                        <li class="nav-item active"><a class="sidebar-link"  href="{{ URL ('/') }}" ><span class="icon-holder"><i class="c-blue-500 ti-home"></i> </span><span class="title">Home</span></a>
                        </li>
                        <li class="nav-item"><a class="sidebar-link" href="{{ URL ('mail') }}"><span class="icon-holder"><i class="c-brown-500 ti-email"></i> </span><span class="title">Email</span></a>
                        </li>
                        <li class="nav-item"><a class="sidebar-link" href="{{ URL ('courses') }}"><span class="icon-holder"><i class="c-blue-500 ti-briefcase"></i> </span><span class="title">{{ __('Courses') }}</span></a>
                        </li>
                        <li class="nav-item"><a class="sidebar-link" href="{{ URL('syllabus') }}"><span class="icon-holder"><i class="c-deep-orange-500 ti-calendar"></i> </span><span class="title">{{ __('Syllabus') }}</span></a>
                        </li><li class="nav-item"><a class="sidebar-link" href="{{ url('student') }}"><span class="icon-holder"><i class="c-deep-purple-500 ti-user"></i> </span><span class="title">{{ __('Student') }}</span></a>
                        </li>
                        <li class="nav-item"><a class="sidebar-link" href="{{ url('all_rolllist') }}"><span class="icon-holder"><i class="c-deep-purple-500 ti-eye"></i> </span><span class="title">{{ __('Role') }}</span></a>
                        </li>
                        <li class="nav-item"><a class="sidebar-link" href="{{ url('report') }}"><span class="icon-holder"><i class="c-deep-purple-500 ti-stats-up"></i> </span><span class="title">{{ __('Reports') }}</span></a>
                        </li>
                        <li class="nav-item"><a class="sidebar-link" href="{{ url('admin') }}"><span class="icon-holder"><i class="c-deep-purple-500 ti-user"></i> </span><span class="title">{{ __('Users List') }}</span></a>
                        </li>
                        <li class="nav-item"><a class="sidebar-link" href="{{ url('tool') }}"><span class="icon-holder"><i class="c-deep-blue-500 ti-layers-alt"></i> </span><span class="title">{{ __('TooL') }}</span></a>
                        </li>
                        {{-- <li class="nav-item"><a class="sidebar-link"  href="{{ URL('fee ') }}"><span class="icon-holder"><i class="c-indigo-500 ti-bar-chart"></i> </span><span class="title">{{ __('Fee') }}</span></a>
                        </li> --}}
                        {{-- <li class="nav-item"><a class="sidebar-link" href="forms.html"><span class="icon-holder"><i class="c-light-blue-500 ti-pencil"></i> </span><span class="title">Forms</span></a>
                        </li>
                        <li class="nav-item dropdown"><a class="sidebar-link" href="ui.html"><span class="icon-holder"><i class="c-pink-500 ti-palette"></i> </span><span class="title">UI Elements</span></a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="dropdown-toggle" href="javascript:void(0);">
                                <span class="icon-holder">
                                    <i class="c-orange-500 ti-layout-list-thumb"></i> 
                                </span>
                                <span class="title">Tables</span>
                                <span class="arrow">
                                    <i class="ti-angle-right"></i>
                                </span>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a class="sidebar-link" href="basic-table.html">
                                        Basic Table
                                    </a>
                                </li>
                                <li>
                                    <a class="sidebar-link" href="datatable.html">
                                        Data Table
                                    </a>
                                </li>
                            </ul>
                        </li>
                            <li class="nav-item dropdown">
                                <a class="dropdown-toggle" href="javascript:void(0);">
                                    <span class="icon-holder">
                                        <i class="c-purple-500 ti-map"></i>
                                    </span>
                                    <span class="title">Maps</span>
                                    <span class="arrow"><i class="ti-angle-right"></i></span>
                                </a>
                                <ul class="dropdown-menu">
                                <li>
                                    <a href="google-maps.html">
                                        Google Map
                                    </a>
                                </li>
                                <li>
                                    <a href="vector-maps.html">
                                        Vector Map
                                    </a>
                                </li>
                            </ul>
                        </li> --}}
                        <!-- <li class="nav-item dropdown">
                            <a class="dropdown-toggle" href="javascript:void(0);">
                                <span class="icon-holder"><i class="c-red-500 ti-files"></i> </span>
                                <span class="title">Homepage Manager</span>
                                <span class="arrow"><i class="ti-angle-right"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a class="sidebar-link" href="{{ url('aboutus') }}">
                                        Aboutus
                                    </a>
                                </li>
                                <li>
                                    <a class="sidebar-link" href="{{ URL('service ') }}">
                                        Services
                                    </a>
                                </li> -->
                                {{-- <li>
                                    <a class="sidebar-link" href="{{ URL('fee ') }}">
                                        Portfolio
                                    </a>
                                </li> --}}
                                <!-- <li>
                                    <a class="sidebar-link" href="{{ URL('review ') }}">
                                        What about customer says?
                                    </a>
                                </li> -->
                            </ul>
                        </li>
                        {{-- <li class="nav-item dropdown">
                            <a class="dropdown-toggle" href="javascript:void(0);">
                                <span class="icon-holder">
                                    <i class="c-teal-500 ti-view-list-alt"></i>
                                </span>
                                <span class="title">Multiple Levels</span> 
                                <span class="arrow"><i class="ti-angle-right"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="nav-item dropdown">
                                    <a href="javascript:void(0);">
                                        <span>Menu Item</span>
                                    </a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a href="javascript:void(0);">
                                        <span>Menu Item</span>
                                        <span class="arrow"><i class="ti-angle-right"></i>
                                        </span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="javascript:void(0);">
                                                Menu Item
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);">
                                                Menu Item
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li> --}}
                    </ul>
                </div>
            </div>




        {{-- <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    @guest
                    @if (Route::has('register'))
                    @endif
                       
                    
                    @else
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ URL ('courses') }}">{{ __('Courses') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ URL('syllabus') }}">{{ __('Syllabus') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('student') }}">{{ __('Student') }}</a>
                        </li>
                    </ul>
                        @endguest
                   

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                           
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" 
                                aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav> --}}
        <div class="page-container">
                <div class="header navbar"><div class="header-container">
                    <ul class="nav-left">
                        <li>
                            <a id="sidebar-toggle" class="sidebar-toggle" href="javascript:void(0);">
                                <i class="ti-menu"></i>
                            </a>
                        </li>
                        <li class="search-box">
                            <a class="search-toggle no-pdd-right" href="javascript:void(0);"><i class="search-icon ti-search pdd-right-10"></i> <i class="search-icon-close ti-close pdd-right-10"></i></a>
                        </li>
                        <li class="search-input">
                            <input class="form-control" type="text" placeholder="Search...">
                        </li>
                    </ul>
                    
                    <ul class="nav-right">
                        <li class="nav-item dropdown">
                            <a  href="#" role="button" aria-haspopup="true" aria-expanded="false" v-pre>
                                    <span class="ti-user"></span>  {{ Auth::user()->name }} 
                            </a>
                        </li>
                        
                        <li class="nav-item dropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                <i class="fa fa-sign-out" aria-hidden="true"></i>{{ __('Logout') }}
                            </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            
                        </li>
                        <li class="nav-item dropdown">
                        <button id = "notif" type="button" class="btn btn-demo" data-toggle="modal" data-target="#myModal2">
                        Show Menu
                         </button>
                        </li>
                    </ul>
                </div>
            </div>
        <main class="py-4">
            <body class="app">
            @yield('content')
        </main>
        </div>
    </div>
        <footer class="bdT ta-c p-30 lh-0 fsz-sm c-grey-600">
                <span>Copyright © 2017 Designed by 
                    <a href="http://dev.codeinsightacademy.com" target="_blank" title="Colorlib">CIA</a>. All rights reserved.
                </span>
            <script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13" type="7fb59fb5529afbf733525e72-text/javascript"></script>
            <script type="7fb59fb5529afbf733525e72-text/javascript">
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-23581568-13');
            </script>
            <script src="{{ asset('public/js/rocket-loader.min.js') }}" ></script>
    
            <script src="{{ asset('public/js/analytics.js') }}" ></script>
            <script src="{{ asset('public/js/vendor.js') }}" ></script>
            <script src="{{ asset('public/js/bundle.js') }}" ></script>
            </footer>
    </div>

    <div class="modal right fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
		<div class="modal-dialog" role="document">
			<div class="modal-content">

				<div class="modal-header">
                <h4 class="modal-title" id="myModalLabel2">Hide Menu</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					
				</div>

				<div class="modal-body">
                <div class="board-menu-content-frame">
                    <p class="error js-file-too-large" style="margin: 12px 0 6px; display:none;">File too large. 10mb limit.</p>
                    <ul class="">
                        <li class="">
                            <a class="" href="{{URL('/changepassword')}}">
                            <span class="board-menu-navigation-item-link-icon js-fill-background-preview" ></span>&nbsp;Change Password</a>
                        </li>
                    </ul>
                    <hr>
                    <a class="board-menu-section-header js-open-activity" href="#">
                        <span class="board-menu-section-header-icon icon-lg icon-activity"></span>
                        <span class="board-menu-section-header-title">Activity</span>
                        <span class="board-menu-section-header-count js-unread-activity-count"></span>
                    </a>
                    <div id="notif1"></div>
                    <a class="show-more js-fill-activity-button js-open-activity" href="#">View all activity…</a>
                </div>
            </div>
        </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div><!-- modal -->

<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$("#notif").click(function(e){
e.preventDefault();
$.ajax({

   type:'GET',

   url:"{{url('/notif')}}",
   success:function(data){
       //alert(data);return false;
        $("#notif1").html(data);
      }
});
});
</script>
</body>
</html>