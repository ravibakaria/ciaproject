@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Courses
                <a class="nav-link" href="{{ URL ('/syllabus') }}">{{ __('Back') }}</a>
                </div>

                <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
                @endif

                
                <form name="myForm" id="myForm"  action="{{ route('syllabus.update', $data['syllabus']->id) }}" method="POST">
                @method('PATCH')
                    @csrf
                    <div class="form-group">
                        <label for="name">Courses Name</label>
                        <select name="course" class="form-control">
                        <option value="">--Select--</option>
                        @foreach ($data['courses'] as $course)
                        <option value="{{$course->id}}"<?php if($data['syllabus']->course_id == $course->id) { ?> selected="selected"<?php } ?>>{{ $course->name }}</option>
                        
                        @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="syllabus">Syllabus</label>
                        <textarea name="syllabus" class="form-control" id="summary-ckeditor" cols="30" rows="10">{{ $data['syllabus']->syllabus }}</textarea>
                    </div>
                    
                    <button type="submit" class="btn btn-primary">Update</button>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<script>
    CKEDITOR.replace( 'summary-ckeditor' );
</script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>
$(document).ready(function () {
$('#myForm').validate({ // initialize the plugin
 rules: {
    course: {
         required: true
        
     }
 },
 messages: {
    course: {
         required: "Select Course"
        
     }
 }
});

});
</script>
@endsection