@extends('layouts.app')

@section('content')
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script> -->
  <style>
       .panel-heading {
        background-color: #eee;
        color: #444;
        cursor: pointer;
        padding: 18px;
        width: 100%;
        border: none;
        text-align: left;
        outline: none;
        font-size: 15px;
        transition: 0.4s;
        }
        .panel-heading:hover {
        background-color: #ccc; 
        }
        .panel-body {
        padding: 0 18px;
        background-color: white;
        overflow: hidden;
        }
  </style>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                <h2>List Of Courses</h2>
                <a class="nav-link" href="{{ URL ('syllabus/create') }}">{{ __('Add Syllabus') }}</a>
                </div>

                <div class="card-body">
                    <style>
                    .uper {
                        margin-top: 40px;
                    }
                    </style>
                    <div class="uper">
                    @if(session()->get('success'))
                        <div class="alert alert-success">
                        {{ session()->get('success') }}  
                        </div><br />
                    @endif
                    @foreach($syllabus as $syllb)
                    <div class="panel-group" id="accordion">
                
                        <div class="panel panel-default">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $syllb->id }}">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    {{ $syllb->name }}
                                </h4>
                            </div>
                            </a>
                            <div id="collapse{{ $syllb->id }}" class="panel-collapse collapse">
                            <div class="panel-body">
                                {!! $syllb->syllabus!!}<br>
                                <a href="{{ route('syllabus.edit',$syllb->id)}}" >Edit</a>
                                <form action="{{ route('syllabus.destroy', $syllb->id)}}" method="post" id ="myForm">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" type="submit" id="submit">Delete</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function() {
  
    $('#myForm').submit(function() {
     
    var c = confirm("Click OK to continue?");
    return c; //you can just return c because it will be true or false
    });
});
</script>

  

@endsection