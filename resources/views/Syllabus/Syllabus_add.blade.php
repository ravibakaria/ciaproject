@extends('layouts.app')

@section('content')

        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Add New Courses
                <a class="nav-link" href="{{ URL ('/syllabus') }}">{{ __('Back') }}</a>
                </div>

                <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
                @endif
                <form name="myForm" id="myForm" action="{{URL ('syllabus') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="name">Courses Name</label>
                        <select name="course" class="form-control">
                        <option value="">--Select--</option>
                        @foreach ($courses as $course)
                        <option value="{{$course->id}}">{{ $course->name }}</option>
                        @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="syllabus">syllabus</label>
                        <textarea name="syllabus" class="form-control" id="summary-ckeditor" cols="30" rows="10"></textarea>
                    </div>
                    
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('vendor/unisharp/laravel-ckeditor/adapters/jquery.js') }}"></script>
<script>
     $('textarea').ckeditor();;
</script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>
$(document).ready(function () {
$('#myForm').validate({ // initialize the plugin
 rules: {
    course: {
         required: true
        
     }
 },
 messages: {
    course: {
         required: "Select Course"
        
     }
 }
});

});
</script>

@endsection