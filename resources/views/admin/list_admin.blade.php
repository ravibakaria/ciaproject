
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Users List') }}
                <a class="nav-link" href="{{ URL ('admin/create') }}">{{ __('Add Users') }}</a>
                
                </div> 

                <div class="card-body">
                        <table class="table-responsive" border="1px soild black">
                           
                                <thead>
                                    <tr>
                                        <th>Sr No.</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Roll</th>
                                        <th colspan="2">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i = 0;?>
                                    @foreach($users as $user)
                                        <?php $i++;?>
                                  
                                        
                                    <tr>
                                        <td>{{$i}}</td>
                                        <td><?php echo $user->name;?></td>
                                        <td><?php echo $user->email;?></td>
                                        <td><?php echo $user->roll_name;?></td>
                                        
                                       
                                        <td><a href="{{ route ('admin.edit',$user->id)}}" class="btn btn-primary">Edit</a></td>
                                       
                                        <!-- <td>   <form action="{{ route ('all_rolllist.destroy', $user->id)}}" method="post" id ="myForm">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger" type="submit" id="submit">Delete</button>
                                            </form> 
                                        </td>  -->
                                            
                                        
                                    </tr>
                                    @endforeach
                                
                                    
                                </tbody>
                                </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
