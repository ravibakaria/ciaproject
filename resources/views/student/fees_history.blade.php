<div class="container">
    
            <div class="card" style="top:0px">
                <div class="card-header">
                <h2>Fees History</h2>
                </div>

                <div class="card-body">
                   
                    <table class="table">
                    <thead>
                        <tr> 
                            <th>SR</th>
                            <th>Paid Date</th>
                            <th> Paid Fees</th>   
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i = 0;
                        ini_set('display_errors', 1);?>
                        @foreach($students as $student)
                        <?php $i++;?>
                        <tr>
                            <td>{{$i}}</td>
                            <td>{{$student->tdate}}</td>
                            <td>{{$student->fees}}</td>   
                        </tr>
                        @endforeach
                       
                    </tbody>
                    </table>
                </div>
            </div>
</div>