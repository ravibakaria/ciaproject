@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
          
            <div class="card">
                <div class="card-header">
                <div class="row">
                <div class="col-md-5">
                <h2>{{$students['sname']->name}}</h2>
                <a class="nav-link" href="{{ URL::previous() }}">{{ __('Back') }}</a>
                </div>
                <div class="col-md-5" style="">
                <?php
                  $mails = DB::table('mailtemplates')->get();
                 // dd($mail);
                   
                  ?>
                   <div class="form-group">
                        <label for="name">Mail</label>
                        <select name="mail" class="form-control">
                        <option value="">--Select--</option>
                        @foreach ($mails as $mail)
                        <option value="{{$mail->id}}">{{ $mail->title }}</option>
                        @endforeach
                        </select>
                    </div>
                  
                </div>
                </div>
                
                @if(session()->get('success'))
                <div class="alert alert-success">
                {{ session()->get('success') }}  
                </div><br />
                @endif
                <input type="hidden" name="s_id" id="s_id" value="{{$students['sname']->id}}"/>
                </div>

                <div class="card-body">
               
                    <table class="table">
                    <thead>
                        <tr>
                            <th>SR</th>
                            <th>Courses</th>
                            <th>Total Fees</th>
                            
                            <th>Balance</th>
                            <th>Join Date</th>
                            <th>Fees History</th>
                            <th>Fees Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i = 0;?>
                        @foreach($students['student'] as $student)
                        <?php $i++;?>
                        <tr>
                            <td>{{$i}}</td>
                            <td><a href="{{ route('courses.show',$student->c_id)}}"> {{$student->name}}</a></td>
                            <td>{{$student->fees}}</td>
                            <td><?php
                            $balance = DB::table('fees')
                              ->select(DB::raw('sum(fees) as fees'))
                              ->where('course_id', '=', $student->c_id)
                              ->where('student_id', '=', $students['sname']->id)
                              ->first();
                             
                            ?>{{$student->fees - $balance->fees }} </td>
                            <td>{{$student->date}}</td>
                            <td>
                            <button type="button" class="btn btn-info fees" data-toggle="modal" data-target="#myModal" lastid = "{{$student->c_id}}" >Fees History</button>
                            </td>
                            <td>
                           <?php 
                            
                            if($student->fees == $balance->fees){?>
                                    <button type="button" class="btn btn-info" disabled   >Pay Fees</button>
                           <?php }else{?>
                            <button type="button" class="btn btn-info fee"  lastid = "{{$student->c_id}}" >Pay Fees</button>

                          <?php }
                            ?>
                           
                            
                            </td>
                           
                        </tr>
                        @endforeach
                       
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


  
  
<div class="container">
 

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
      <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
        <h4 class="modal-title"></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
         
        </div>
        <div class="modal-body">
          <div id ="fees">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
      
    </div>
  </div>
  <!--Model2--->
  <div id="myModal1" style="display:none;">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
        <h4 class="modal-title"></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
         
        </div>
        <div class="modal-body">
         
          <form id="form1" action="{{URL ('fee') }}" method="POST">
              @csrf
              <div class="form-group">
                  
                  <input type="hidden" name = "sid" class="form-control" id="name1"  >
              </div>
              <div class="form-group">
                  
                  <input type="hidden" name = "cid" class="form-control" id="course1" >
              </div>
              
              
              <div class="form-group">
              <label for="fees">Pay Date</label>
                  <input type="date" name = "pdate" class="form-control" id="pay" placeholder="Enter Join Date">
              </div>
              <div class="form-group">
              <label for="fees">Fees</label>
                  <input type="text" name = "fees" class="form-control" id="fees" placeholder="Enter Fees">
              </div>
              <button type="submit" class="btn btn-primary">Submit</button>
          </form>
          
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default close" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>

<!--Model3--->
<div id="myModal3" style="display:none;">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
        <h4 class="modal-title"></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
         
        </div>
        <div class="modal-body">
         
          <form id="form1" action="{{URL ('mail/send') }}" method="POST">
              @csrf
              
              <input type="hidden" name = "name" value="{{$students['sname']->name}}" >
              
              <div class="form-group">
              <label for="fees">To</label>
                  <input type="text" name = "to" class="form-control" id="to"  value="{{$students['sname']->email}}" >
              </div>
                <div class="form-group">
                <label for="fees">Subject</label>
                    <input type="text" name = "subject" class="form-control" id="subject" readonly >
                </div>
              <div class="form-group">
              <label for="fees">Mail body</label>
              <p>Note:-Please use &lt;br&gt; to break line</P>
                <textarea name="mailbody" id="mailbody" cols="30" rows="8"class="form-control"></textarea>
                 
              </div>
              <button type="submit" class="btn btn-primary">send</button>
          </form>
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default close" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script>


   

$.ajaxSetup({

    headers: {

        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

    }

});



$(".fees").click(function(e){

    

    e.preventDefault();
   
    var c_id =$(this).attr('lastid')
    //alert(c_id);


     var s_id = $("input[name=s_id]").val();



    $.ajax({

       type:'POST',

       url:"{{url('/ajaxRequest')}}",

       data:{'s_id':s_id, 'c_id':c_id},

       success:function(data){
            $("#fees").html(data);
          

       }

    });



});

$(".fee").click(function(e){

    

e.preventDefault();

var c_id =$(this).attr('lastid')

//alert(c_id);


var s_id = $("input[name=s_id]").val();
$("#name1").val(s_id);
$("#course1").val(c_id);
$('#myModal1').css("display","block");
$('#myModal1').css("position","relative");
$('#myModal1').css("top","-200px");



});

$(".close").click(function(e){
  $('#form1')[0].reset();
  $('#myModal1').css("display","none");
  $('#myModal3').css("display","none");


});

$('select').on('change', function() {
  mail_id =  this.value ;
  $.ajax({

       type:'POST',

       url:"{{url('/ajaxRequestmail')}}",

       data:{'id':mail_id},

       success:function(data){
        var obj = JSON.parse(data);
        // alert(data);
        $('#mailbody').val(obj.body);
        $('#subject').val(obj.subject);
        $('#myModal3').css("display","block");
        $('#myModal3').css("position","relative");
        $('#myModal3').css("top","-200px");
          

       }

    });
});
</script>


  

@endsection