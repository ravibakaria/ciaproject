@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">New Admission
                <a class="nav-link" href="{{ URL::previous() }}">{{ __('Back') }}</a>
                </div>

                <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
                @endif
                <form name="myForm" id="myForm" action="{{URL ('student') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="name">Student Name</label>
                        <input type="text" id = "name" name = "name" class="form-control"  aria-describedby="emailHelp" placeholder="Enter Student Name">
                    </div>
                    <div class="form-group">
                        <label for="name">Student Email</label>
                        <input type="email" id = "email" name = "email" class="form-control"  aria-describedby="emailHelp" placeholder="Enter Student Name">
                    </div>
                    <div class="form-group">
                        <label for="name">Student Phone No.</label>
                        <input type="text" name = "phone" class="form-control"  aria-describedby="emailHelp" placeholder="Enter Student Phone No.">
                    </div>
                    <div class="form-group">
                        <label for="name">Courses Name</label>
                        <select name="course" class="form-control">
                        <option value="">--Select--</option>
                        @foreach ($courses as $course)
                        <option value="{{$course->id}}">{{ $course->name }}</option>
                        @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                    <label for="fees">Join Date</label>
                        <input type="date" name = "jdate" class="form-control"  placeholder="Enter Join Date">
                    </div>
                    <div class="form-group">
                    <label for="fees">Fees</label>
                        <input type="text" name = "fees" class="form-control" id="fees" placeholder="Enter Fees">
                    </div>
                   
                    <div class="form-group">
                        <label for="address">Address</label>
                        <textarea  class="form-control" name ="address"></textarea>
                    </div>
                    <div class="form-group">
                    <label for="gender">Gender</label>
                        <input type="radio" name = "gender" value="1" >Male
                        <input type="radio" name = "gender" value ="0">Female
                    </div>
                    <div class="form-group">
                    <label for="city">City</label>
                        <input type="text" name = "city" class="form-control">
                    </div>
                    <div class="form-group">
                    <label for="pin">Pin Code</label>
                        <input type="text" name = "pin" class="form-control">
                    </div>
                    <div id="refe" class="form-group" style="display:none">
                            <label for="name">Reference Name</label>
                    <select name="refe"  class="form-control" >
                        <option value="">--Select--</option>
                        @foreach ($students as $student)
                        <option value="{{$student->id}}">{{ $student->name }}</option>
                        @endforeach
                    </select>
                    </div>
                    <button type="button" class="btn btn-primary ref">Reference</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                    
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
    $(".ref").click(function(){
        
        $("#refe").css("display","block");
        $(".ref").css("display","none");
    });
    
});
</script>
       <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
       <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
   <script>
    $(document).ready(function () {
    $('#myForm').validate({ // initialize the plugin
        rules: {
            name: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            phone: {
                required: true
            },
            jdate: {
                required: true
            },
            fees: {
                required: true
            },
            course: {
                required: true
            
            },
            address: {
                required: true
            
            },
            pin: {
                required: true
            
            },
            gender: {
                required: true
            },
            city:{
                required: true
            }
        },
        messages:{
            name: {
                required: "Name is required"
            },
            email: {
                required: "Email-ID is required"
                
            },
            phone: {
                required: "Phone no. is required"
            },
            jdate: {
                required: "Joining-Date is required"
            },
            fees: {
                required: "Fees should nerver black"
            },
            course: {
                required: "Course is required"
            },
            address: {
                required: "Address is required"
            
            },
            pin: {
                required: "Pincode is required"
            
            },
            gender: {
                required: "Please select Gender"
            },
            city:{
                required: "City is required"
            }
        }
    });
   
});
</script>
@endsection