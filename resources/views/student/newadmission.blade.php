@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">New Admission
                <a class="nav-link" href="{{ URL::previous() }}">{{ __('Back') }}</a>
                </div>   

                <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
                @endif
                <form name="myForm" id="myForm" action="{{ URL ('admissionaction', $data['student']->id) }}" method="POST">
                @csrf
               
                    <div class="form-group">
                        
                        <input type="hidden" name = "name" class="form-control" id="name" aria-describedby="emailHelp" placeholder="Enter Student Name"  value="{{$data['student']->name}}">
                    </div>
                    <div class="form-group">
                       
                        <input type="hidden" name = "email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter Student Name"  value="{{$data['student']->email}}"  >
                    </div>
                    <div class="form-group">
                       
                        <input type="hidden" name = "phone" class="form-control" id="phone" aria-describedby="emailHelp" placeholder="Enter Student Phone No."  value="{{$data['student']->phone}}" >
                    </div>
                    <div class="form-group">
                        <label for="name">Courses Name</label>
                        <select name="course" class="form-control">
                        <option value="">--Select--</option>
                        @foreach ($data['courses'] as $course)
                        <option value="{{$course->id}}">{{ $course->name }}</option>
                        
                        @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                    <label for="fees">Join Date</label>
                        <input type="date" name = "jdate" class="form-control" id="jdate" placeholder="Enter Join Date"  >
                    </div>
                    <div class="form-group">
                    <label for="fees">Fees</label>
                        <input type="text" name = "fees" class="form-control" id="fees" placeholder="Enter Fees" >
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>
$(document).ready(function () {
$('#myForm').validate({ // initialize the plugin
 rules: {
     name: {
         required: true
        
     },
     email: {
         required: true,
         email: true
     },
     phone: {
         required: true
     },
     jdate: {
         required: true
     },
     fees: {
         required: true
     },
     course: {
         required: true
     },
 }
});

});
</script>

@endsection