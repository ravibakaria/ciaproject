@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">New Admission
                <a class="nav-link" href="{{ URL::previous() }}">{{ __('Back') }}</a>
                </div>

                <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
                @endif
                <form name="myForm" id="myForm" action="{{ route('student.update', $data['student']->id) }}" method="POST">
                @csrf
                @method('PATCH')
                    <div class="form-group">
                        <label for="name">Student Name</label>
                        <input type="text" name = "name" class="form-control" id="name" aria-describedby="emailHelp" placeholder="Enter Student Name"  value="{{$data['student']->name}}">
                    </div>
                    <div class="form-group">
                        <label for="name">Student Email</label>
                        <input type="email" name = "email" class="form-control" id="name" aria-describedby="emailHelp" placeholder="Enter Student Name"  value="{{$data['student']->email}}"  >
                    </div>
                    <div class="form-group">
                        <label for="name">Student Phone No.</label>
                        <input type="text" name = "phone" class="form-control" id="name" aria-describedby="emailHelp" placeholder="Enter Student Phone No."  value="{{$data['student']->phone}}" >
                    </div>
                    <div class="form-group">
                        <label for="address">Address</label>
                        <textarea  class="form-control" name ="address">{{$data['student']->address}}</textarea>
                    </div>
                    <div class="form-group">
                    <label for="gender">Gender</label>
                        <input type="radio" name = "gender" value="1" >Male
                        <input type="radio" name = "gender" value ="0">Female
                    </div>
                    <div class="form-group">
                    <label for="city">City</label>
                        <input type="text" name = "city" class="form-control" value="{{$data['student']->city}}">
                    </div>
                    <div class="form-group">
                    <label for="pin">Pin Code</label>
                        <input type="text" name = "pin" class="form-control" value="{{$data['student']->pin}}">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>
$(document).ready(function () {
$('#myForm').validate({ // initialize the plugin
 rules: {
     name: {
         required: true
        
     },
     email: {
         required: true,
         email: true
     },
     phone: {
         required: true
     },
     jdate: {
         required: true
     },
     fees: {
         required: true
     },
     course: {
         required: true
     },
        address: {
            required: true
        
        },
        pin: {
            required: true
        
        },
        gender: {
            required: true
        },
        city:{
            required: true
        }
 },
 messages:{
            name: {
                required: "Name is required"
            },
            email: {
                required: "Email-ID is required"
                
            },
            phone: {
                required: "Phone no. is required"
            },
            jdate: {
                required: "Joining-Date is required"
            },
            fees: {
                required: "Fees should nerver black"
            },
            course: {
                required: "Course is required"
            }
        }
});

});
</script>

@endsection