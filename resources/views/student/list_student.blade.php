@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                <h2>List Of Students</h2>
                <a class="nav-link" href="{{ URL ('student/create') }}">{{ __('New Admission') }}</a>
                
                <div class="panel-body">
 
                <!-- <a href="{{ url('downloadExcel/xls') }}"><button class="btn btn-success">Download Semple Excel xls</button></a> -->
                <a href="{{ asset('public/image/sample.xlsx') }}" download><button class="btn btn-success">Download Sample Excel xls</button></a>

                <form style="border: 4px solid #a1a1a1;margin-top: 15px;padding: 10px;" action="{{ url('importExcel') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                    @csrf

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @if (Session::has('success'))
                        <div class="alert alert-success">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <p>{{ Session::get('success') }}</p>
                        </div>
                    @endif

                    <input type="file" name="import_file" />
                    <button class="btn btn-primary">Import File</button>
                </form>

                </div>
                </div>
                <div class="card-body">
                    <style>
                    .uper {
                        margin-top: 40px;
                    }
                    .tr ,td,th{
                        text-align:inherit;
                    }
                    
                    </style>
                    <div class="uper">
                    
                    <table id="example" class="display nowrap" style="width:100%">  
                        <thead>
                            <tr>
                                <th>SR</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Reference</th>
                                <th>Phone No.</th>
                                <th>Total Courses</th>
                                <th >Total Fees</th>
                                <th>Balance</th>
                                
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 0;?>
                            @foreach($students as $student)
                            <?php $i++; 
                                $b = DB::select("SELECT sum(fees) as pfees FROM `fees` WHERE student_id='$student->id'");
                            ?>
                            <tr>
                                <td>{{$i}}</td>
                                <td><a href="{{ URL ('student_detail',$student->id)}}">{{$student->name}} </a></td>
                                <td>{{$student->email}}</td>
                                <td>
                                    <?php
                                    if($student->reference != "null"){
                                    $ref = DB::select("SELECT name FROM `students` WHERE id='$student->reference'");
                                    foreach ($ref as $value) {
                                        echo $value->name;
                                    }
                                    }
                                    // print_r($ref);
                                    //$ref = Student::find($student->reference)->first();
                                    //echo  $ref['0']['name'];
                                    ?></td>
                                <td>{{$student->phone}}</td>
                                <td style="text-align:center">{{$student->c_count}}</td>
                                <td>{{$student->fees1}}</td>
                                <td style="text-align:center">
                                {{$student->fees1 - $b[0]->pfees}}
                                </td>
                                
                                    {{-- <form action="{{ URL ('softdestroy', $student->id)}}" method="post" id ="myForm">
                                    @csrf
                                    @method('PATCH')
                                    <button class="btn btn-danger" type="submit" id="submit">Delete</button>
                                    </form> --}}
                               
                            </tr>
                        <?php //}?>
                        @endforeach
                    </tbody>
                    </table>
                  
                </div>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

<script src = "https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable({
        "scrollX": true,
        dom: 'Blfrtip',
        buttons: [
            {
                extend: 'excelHtml5',
                title: 'Student export'
            },
            {
                extend: 'csvHtml5',
                title: 'Student export'
            }
        ]
    } );
    $('#myForm').submit(function() {
     
    var c = confirm("Click OK to continue?");
    return c; //you can just return c because it will be true or false
    });
});

</script>

</div>
</main>
</body>

@endsection