@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                <h2>Admin Panel</h2>
                <a class="nav-link" href="{{ URL::previous() }}">{{ __('Back') }}</a>
                </div>
                <div class="card-body">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />
                    @endif
                   
                    <form name="myForm" id="myForm" action="{{ route('all_rolllist.update',$roll->id)}}" method="POST">
                            @csrf
                            @method('PATCH')
                      
                        <div class="form-group">
                            <label for="name">Role Name</label>
                           
                        <input type="text" id = "name" name = "name" class="form-control"  aria-describedby="emailHelp" value="{{$roll->roll_name}}">
                       
                        </div>
                        
                        <button type="submit" class="btn btn-primary">Submit</button>
                        
                    </form>
                   
                </div>
            </div>
        </div>
    </div>
</div>
   
    
           <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
           <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
       <script>
        $(document).ready(function () {
        $('#myForm').validate({ // initialize the plugin
            rules: {
                name: {
                    required: true
                }
                
            },
            messages:{
                name: {
                    required: "Name is required"
                },
               
        });
       
    });
    </script>
    @endsection