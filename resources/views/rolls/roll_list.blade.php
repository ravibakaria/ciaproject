@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                <h2> {{$rolls[0]['name']}}</h2>
                
                </div>
                <div class="card-body">
                    <style>
                    .uper {
                        margin-top: 40px;
                    }
                    .tr ,td,th{
                        text-align:inherit;
                    }
                    </style>
                    <div class="uper">
                    @if(session()->get('success'))
                        <div class="alert alert-success">
                        {{ session()->get('success') }}  
                        </div><br />
                    @endif
                    <table class="table-responsive">
                           
                    <thead>
                        <tr>
                            <th>SR</th>
                            <th>Name</th>
                            <th colspan="2">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i = 0;?>
                        @foreach($rolls as $roll)
                        <?php $i++; ?>
                        <tr>
 
                            <td>{{$i}}</td>
                            <td>{{$rolls[0]['name']}} </a></td>
                            
                           
                            <td><a href="{{ route('admin.edit',$rolls[0]['id'])}}" class="btn btn-primary">Edit</a></td>
                            <td > <a href="{{ URL ('user',$rolls[0]['id'])}}" class="btn btn-danger">Delete</a></td>
                                
                            </td>
                        </tr>
                    <?php //}?>
                        @endforeach
                    </tbody>
                    </table>
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection