@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                <h2>Admin Panel</h2>
                <a class="nav-link" href="{{ URL ('all_rolllist/create') }}">{{ __('Add Role') }}</a>
                </div>
                <div class="card-body">
                    <style>
                    .uper {
                        margin-top: 40px;
                    }
                    .tr ,td,th{
                        text-align:inherit;
                    }
                    </style>
                    <div class="uper">
                    @if(session()->get('success'))
                        <div class="alert alert-success">
                        {{ session()->get('success') }}  
                        </div><br />
                    @endif
                    <table class="table-responsive">
                           
                    <thead>
                        <tr>
                            <th>SR</th>
                            <th>Name</th>
                            <th colspan="2">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php $i = 0;?>
                        @foreach($rolls as $roll)
                            <?php $i++;?>
                      
                            
                        <tr>

                            <td><?php echo $i;?></td>
                            <td>{{$roll->roll_name}} </a></td>
                            
                           
                            <td><a href="{{ route('all_rolllist.edit',$roll->id)}}" class="btn btn-primary">Edit</a></td>
                            {{-- <td > <a href="{{ URL ('softdestroy',$roll->id)}}" class="btn btn-danger">Delete</a></td> --}}
                            <td>   <form action="{{ route ('all_rolllist.destroy', $roll->id)}}" method="post" id ="myForm">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" type="submit" id="submit">Delete</button>
                                </form> 
                            </td>
                                
                            </td>
                        </tr>
                        @endforeach
                    
                        
                    </tbody>
                    </table>
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection