<div class="container">
    @foreach($logs as $log)
    <div>
        <p><b><?php echo ucfirst($log->name)?></b>&nbsp;{{$log->msg}}<br>
        <?php $date=date_create($log->created_at);
        echo date_format($date,'M d '). " at". date_format($date,' h:i a');
            ?></p>
        
    </div>
    <hr>
    @endforeach           
</div>