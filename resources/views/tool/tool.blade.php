@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" style ="width: 30%"> 
                    <div class="form-group">
                        <label for="name">Courses Name</label>
                        <select id = "course" name="course" class="form-control">
                        <option value="">--Select--</option>
                        @foreach ($courses as $course)
                        <option value="{{$course->id}}">{{ $course->name }}</option>
                        @endforeach
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <h1> Course detail</h1>
                        Name : PHP <br>
                        Durestion : 45 <br>
                        Fees : 3500<br>
                    </div>
                    <div class="col-md-6" style = "margin-top: -9%;">
                    <h1> Related Course </h1>
                        Name : Java <br>
                        Durestion : 45 <br>
                        Fees : 3500<br>
                    </div>
                </div> 
            </div>
        </div>
    </div>
</div>
<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$('#course').on('change', function() {
    var id = this.value;
    $.ajax({
        type:'POST',
        url:"{{url('/ajaxRequestToCourses')}}",
        data:{'c_id':id, },
        success:function(data){
           // alert(data.course.name);return false;
            var course = '';
            var rcourse = '';
            course += `<div class="row">

                    <div class="col-md-6">${data.course.name}</div>
                    <div class="col-md-6">hiii</div>
            </div>`;
           
        }
    });
});

</script>
@endsection